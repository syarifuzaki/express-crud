# Express CRUD


## Installation

1. Clone this repo
2. Run `npm install`. If you don't have Node.js. Install [from here](https://nodejs.org/en/download/)
3. Run mysql server and create new database
4. Set up the `.env` file configuration properly
5. Run `npx sequilize migrate` for migration
6. Run `npx sequilize db:seed:all` for seeding dummy data
7. Run server with `npm run start` or `npm run server` for nodemon