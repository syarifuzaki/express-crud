var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');

const { Post } = require('../models');

/* GET posts listing. */
router.get('/', async (req, res, next) => {
  const page = Number.parseInt(req.query.page);
  const limit = Number.parseInt(req.query.limit);

  let startItem = 0;
  if (!Number.isNaN(page) && page > 0) {
    startItem = page;
  }

  let size = 10
  if (!Number.isNaN(limit) && limit > 0 && limit < 10) {
    size = limit;
  }

  const posts = await Post.findAndCountAll({
    limit: size,
    offset: (startItem - 1) * size
  });
  return res.status(200).json({
    success: true,
    data: posts.rows,
    limit: limit,
    currentPage: page,
    total: posts.count,
    lastPage: Math.ceil(posts.count / size),
  });
});

router.get('/:id', async (req, res, next) => {
  const id = req.params.id;
  const post = await Post.findByPk(id);
  if (!post) {
    return res.status(404).json({
      success: false,
      message: 'Tidak ditemukan'
    });
  }
  return res.status(200).json({
    success: true,
    data: post
  });
});

router.post('/', async (req, res, next) => {
  const title = req.body.title
  const description = req.body.description
  const category = req.body.category
  const status = 0;
  let image = req.body.image
  if (!title) return res.status(400).json({ success: false, message: 'Judul harus ada' });
  if (!description) return res.status(400).json({ success: false, message: 'Deskripsi harus ada' });
  if (!category) return res.status(400).json({ success: false, message: 'Kategori harus ada' });

  if (image && image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)) {
    const extension = image.split(';')[0].split('/')[1]
    const filePath = `files/${Date.now()}.${extension}`;
    const buffer = Buffer.from(image.split(',')[1], "base64");
    fs.writeFileSync(filePath, buffer);
    image = filePath;
  }

  const post = await Post.create({ title, description, status, image, category });

  return res
    .status(201)
    .json({
      success: true,
      data: post,
    })
});

router.put('/:id', async (req, res, next) => {
  const id = req.params.id;
  const title = req.body.title
  const description = req.body.description
  const category = req.body.category

  let image = req.body.image
  if (!title) return res.status(400).json({ success: false, message: 'Judul harus ada' });
  if (!description) return res.status(400).json({ success: false, message: 'Deskripsi harus ada' });
  if (!category) return res.status(400).json({ success: false, message: 'Kategori harus ada' });

  if (image && image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)) {
    const extension = image.split(';')[0].split('/')[1]
    const filePath = `files/${Date.now()}.${extension}`;
    const buffer = Buffer.from(image.split(',')[1], "base64");
    fs.writeFileSync(filePath, buffer);
    image = filePath;
  }

  let post = await Post.findByPk(id);

  if (!post) {
    return res.status(404).json({ success: false, message: 'Tidak ditemukan' });
  }

  let body = {
    title: title,
    description: description,
    category: category,
    image: image ? image : post?.image
  }

  post = await post.update(body);
  return res.status(201).json({
    success: true,
    data: post
  });
});

router.delete('/:id', async (req, res, next) => {
  const id = req.params.id;

  let post = await Post.findByPk(id);

  if (!post) {
    return res.status(404).json({ success: false, message: 'Tidak ditemukan' });
  }

  await post.destroy();

  return res.status(201).json({
    success: true,
    message: 'Berhasil di hapus' 
  });
});

router.post('/:id/read', async (req, res, next) => {
  const id = req.params.id;

  let post = await Post.findByPk(id);

  if (!post) {
    return res.status(404).json({ success: false, message: 'Tidak ditemukan' });
  }

  post = await post.update({ status: 1 });
  return res.status(201).json({ success: true, mesage: 'Berhasil dibaca' });
});

module.exports = router;
