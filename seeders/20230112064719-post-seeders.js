'use strict';
const { faker } = require('@faker-js/faker');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     * 
    */
    const data = [];
    for (let i = 0; i < 5; i++) {
      const item = {
        title: faker.lorem.sentence(7),
        description: faker.lorem.paragraphs(20),
        status: faker.datatype.boolean(),
        image: faker.image.abstract(200, 200, true),
        category: faker.lorem.word(),
        createdAt: new Date(),
        updatedAt: new Date(),
      }

      data.push(item);
    }
    return queryInterface.bulkInsert('posts', data, {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('posts', null, {});
  }
};